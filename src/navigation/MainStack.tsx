import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { AboutPage, GamePage } from '../screens';
import styled from 'styled-components/native';

const MainStack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default () => {
    return (
        <DrawerNavigator>
            <MainStack.Screen name="Game" component={GamePage} />
            <MainStack.Screen name="About" component={AboutPage} />
        </DrawerNavigator>
    );
};

const DrawerNavigator = styled(Drawer.Navigator).attrs(({ theme }) => ({
    drawerStyle: {
        backgroundColor: theme.colors.app,
    },
    screenOptions: {
        headerShown: false,
    },
}))``;
