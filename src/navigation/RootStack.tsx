import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import MainStack from './MainStack';

const RootStack = createStackNavigator();

export default () => {
    return (
        <NavigationContainer>
            <RootStack.Navigator headerMode="none">
                <RootStack.Screen name={'Main'} component={MainStack} />
            </RootStack.Navigator>
        </NavigationContainer>
    );
};
