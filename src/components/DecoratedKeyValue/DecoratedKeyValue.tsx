import React from 'react';
import styled from 'styled-components/native';
import { DefaultText } from '@components/_universal/Typography';
import { IconType } from '@@types/CommonTypes';
import IconManager from '@components/_universal/IconManager/IconManager';
import WhiteBox from '@components/_universal/WhiteBox';

interface IProps {
    title: string;
    value: string | number;
    icon?: {
        name: IconType;
        size?: number;
    };
}

export default ({ title, value, icon }: IProps) => {
    return (
        <Wrapper>
            <TitleBox>
                <DefaultText>{title}</DefaultText>
            </TitleBox>
            <Right>
                <DefaultText numberOfLines={1} ellipsizeMode={'tail'}>
                    {value}
                </DefaultText>
                {icon && (
                    <IconManager
                        type={icon.name}
                        size={icon.size}
                        centerProps
                    />
                )}
            </Right>
        </Wrapper>
    );
};

const Wrapper = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

const Right = styled(WhiteBox)`
    flex: 1;
    flex-shrink: 0;
    flex-direction: row;
    justify-content: space-between;
`;

const TitleBox = styled(WhiteBox)`
    flex: 2.5;
`;
