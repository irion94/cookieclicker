import React from 'react';
import styled from 'styled-components/native';
import { observer } from 'mobx-react';
import bakeryStore from '@state/Bakery.store';
import DecoratedKeyValue from '@components/DecoratedKeyValue/DecoratedKeyValue';

export default observer(() => {
    return (
        <Wrapper>
            <DecoratedKeyValue
                title={'Points:'}
                value={bakeryStore.points}
                icon={{ name: 'Cookie', size: 20 }}
            />
            <DecoratedKeyValue
                title={'Level:'}
                value={bakeryStore.level}
                icon={{ name: 'Star', size: 20 }}
            />
            <DecoratedKeyValue
                title={'Robots'}
                value={bakeryStore.robots}
                icon={{ name: 'Robot', size: 20 }}
            />
            <DecoratedKeyValue
                title={'Robots Efficiency:'}
                value={`${bakeryStore.getEfficiency}/s`}
                icon={{ name: 'Flash', size: 20 }}
            />
            <DecoratedKeyValue
                title={'Next level: '}
                value={bakeryStore.getNextLevel}
                icon={{ name: 'LevelUp', size: 20 }}
            />
        </Wrapper>
    );
});

const Wrapper = styled.View`
    background-color: ${({ theme }) => theme.colors.box};
    border-radius: ${({ theme }) => theme.borderRadius.default}px;
    padding: ${({ theme }) => theme.spacing.medium}px;
    margin: 0 ${({ theme }) => theme.spacing.wide}px;
`;
