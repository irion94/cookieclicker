import styled from 'styled-components/native';
import Animated from 'react-native-reanimated';

export const CookieButton = styled.TouchableOpacity.attrs({
    activeOpacity: 1,
})<{
    cookieSize?: number;
}>`
    height: ${({ cookieSize }) => cookieSize || 20}px;
    width: ${({ cookieSize }) => cookieSize || 20}px;
    border-radius: ${({ cookieSize }) =>
        (cookieSize && cookieSize / 2) || 10}px;
    elevation: 1;
    z-index: 1;
`;

export const Wrapper = styled(Animated.View)`
    width: 100%;
    justify-content: center;
    align-items: center;
`;
