import React from 'react';
import * as Styled from './Cookie.styled';
import { observer } from 'mobx-react';
import IconManager from '@components/_universal/IconManager/IconManager';
import { useRootStore } from '@utils/hooks/useRootContext';
import { normalize } from '@utils/functions/normalize';
import { useAnimation } from '@utils/hooks/useAnimation';

export default observer(() => {
    const { bakery } = useRootStore();
    const cookieSize = normalize(250);
    const { scale, scaleAnimation } = useAnimation();

    return (
        <Styled.Wrapper style={{ transform: [{ scale }] }}>
            <Styled.CookieButton
                onPressIn={() => scaleAnimation()}
                onPress={() => {
                    bakery.addPoints(bakery.multiplier);
                }}
                cookieSize={cookieSize}
            >
                <IconManager type={'Cookie'} size={cookieSize} centerProps />
            </Styled.CookieButton>
        </Styled.Wrapper>
    );
});
