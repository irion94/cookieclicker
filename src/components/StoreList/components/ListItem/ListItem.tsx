import React from 'react';
import styled from 'styled-components/native';
import { IStoreItemValue } from '@state/StoreItem.store';
import { observer } from 'mobx-react';
import WhiteBoxText from '@components/_universal/WhiteBoxText/WhiteBoxText';
import IconManager from '@components/_universal/IconManager/IconManager';
import { useRootStore } from '@utils/hooks/useRootContext';
import { IconType } from '@@types/CommonTypes';
import WhiteBox from '@components/_universal/WhiteBox';
import DisabledItem from '@components/DisabledItem/DisabledItem';
import { normalize } from '@utils/functions/normalize';

export default observer(
    ({ name, cost, type, min_lvl, icon_name, bumpMinLvl }: IStoreItemValue) => {
        const { bakery } = useRootStore();

        const toLow = bakery.level < min_lvl;
        const notEnoughPoints = bakery.points < cost;

        const onPress = () => {
            if (!toLow)
                switch (type) {
                    case 'multiply': {
                        bakery.addMultiplier(cost, bumpMinLvl);
                        break;
                    }
                    case 'robot': {
                        bakery.addRobots(cost);
                        bumpMinLvl();
                        break;
                    }
                    case 'robot_upgrade': {
                        bakery.robotUpgrade(cost);
                        bumpMinLvl();
                        break;
                    }
                }
        };

        return (
            <Wrapper onPress={onPress} disabled={toLow || notEnoughPoints}>
                <WhiteBoxText>{name}</WhiteBoxText>
                <WhiteBoxText>Cost: {cost}</WhiteBoxText>
                {icon_name && (
                    <IconBox>
                        <IconWrapper>
                            <IconManager
                                type={icon_name as IconType}
                                size={60}
                            />
                        </IconWrapper>
                    </IconBox>
                )}
                <DisabledItem
                    isShown={toLow}
                    text={`Unlock at level: ${min_lvl}`}
                />
                <DisabledItem
                    isShown={!toLow && notEnoughPoints}
                    text={`Not enough points!`}
                />
            </Wrapper>
        );
    },
);

const Wrapper = styled.TouchableOpacity<{ disabled?: boolean }>`
    height: ${normalize(250)}px;
    width: ${normalize(250)}px;
    background-color: ${({ theme }) => theme.colors.box};
    margin: ${({ theme }) => theme.spacing.medium}px;
    padding: ${({ theme }) => theme.spacing.medium}px;
    border-radius: ${({ theme }) => theme.borderRadius.default}px;
    position: relative;
`;

const IconBox = styled(WhiteBox)`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

const IconWrapper = styled.View`
    padding: ${({ theme }) => theme.spacing.medium}px;
    background-color: ${({ theme }) => theme.colors.box};
    border-radius: ${({ theme }) => theme.borderRadius.default}px;
`;
