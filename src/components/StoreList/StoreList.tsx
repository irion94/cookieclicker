import React from 'react';
import { FlatList } from 'react-native';
import ListItem from '@components/StoreList/components/ListItem/ListItem';
import { observer } from 'mobx-react';
import { useRootStore } from '@utils/hooks/useRootContext';
import { toJS } from 'mobx';
import styled from 'styled-components/native';
import { normalize } from '@utils/functions/normalize';

export default observer(() => {
    const { store } = useRootStore();

    return (
        <Wrapper>
            <FlatList
                data={(store.articles as unknown[]) as any[]}
                renderItem={({ item }) => (
                    <ListItem {...item} bumpMinLvl={item?.bumpMinLvl} />
                )}
                extraData={toJS(store.articles)}
                keyExtractor={(item) => item.id.toString()}
                snapToInterval={normalize(250)}
                decelerationRate={'fast'}
                snapToAlignment={'start'}
                showsHorizontalScrollIndicator={false}
                horizontal
                bounces
                pagingEnabled
            />
        </Wrapper>
    );
});

export const Wrapper = styled.View``;
