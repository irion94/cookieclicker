import styled, { css } from 'styled-components/native';

export default styled.View<{ center?: boolean }>`
    background-color: white;
    border-radius: 3px;
    margin: 3px 5px;
    padding: 1px 5px;
    ${({ center }) =>
        center &&
        css`
            align-items: center;
            justify-content: center;
        `}
`;
