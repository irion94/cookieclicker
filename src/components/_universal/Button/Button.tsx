import React, { ReactChild, ReactChildren } from 'react';
import styled from 'styled-components/native';
import { DefaultText } from '@components/_universal/Typography';
import { TouchableOpacityProps } from 'react-native';

interface IProps extends TouchableOpacityProps {
    children:
        | ReactChild
        | ReactChildren
        | ReactChild[]
        | ReactChildren[]
        | string
        | number;
}

export default ({ children, ...props }: IProps) => {
    return (
        <Touchable {...props}>
            <DefaultText>{children}</DefaultText>
        </Touchable>
    );
};

const Touchable = styled.TouchableOpacity`
    width: 100%;
    height: 50px;
    background-color: ${({ theme }) => theme.colors.button};
    border-radius: ${({ theme }) => theme.borderRadius.small}px;
    justify-content: center;
    align-items: center;
`;
