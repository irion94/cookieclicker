import React, { ReactChildren, ReactChild } from 'react';
import WhiteBox from '@components/_universal/WhiteBox';
import { DefaultText } from '@components/_universal/Typography';

interface IProps {
    children:
        | ReactChild
        | ReactChildren
        | ReactChild[]
        | ReactChildren[]
        | string
        | number;
}

export default ({ children }: IProps) => {
    return (
        <WhiteBox>
            <DefaultText>{children}</DefaultText>
        </WhiteBox>
    );
};
