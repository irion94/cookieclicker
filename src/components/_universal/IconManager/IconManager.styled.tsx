import styled, { css } from 'styled-components/native';

export const IconHolder = styled.View<{
    rotate?: number;
    centerProps?: boolean;
}>`
    ${({ centerProps }) =>
        centerProps &&
        css`
            align-self: center;
        `};
    ${({ rotate }) =>
        rotate &&
        css`
            transform: rotate(${rotate}deg);
        `};
`;
