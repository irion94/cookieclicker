import * as React from 'react';
import * as Icons from '@shared/SVGs';
import { IconType, ViewStyleProp } from '@@types/CommonTypes';
import { IconHolder } from './IconManager.styled';
import { palette } from '@shared/theme/config';

interface IProps {
    type?: IconType;
    fill?: keyof typeof palette;
    size?: number;
    rotate?: number;
    style?: ViewStyleProp;
    centerProps?: boolean;
}

const IconManager: React.FC<IProps> = ({
    type,
    fill,
    size,
    rotate,
    style,
    centerProps,
}) => {
    const Icon = Icons[type || 'CloseIcon'];

    return (
        <IconHolder style={style} rotate={rotate} centerProps={centerProps}>
            <Icon size={size} fill={fill} />
        </IconHolder>
    );
};

export default IconManager;
