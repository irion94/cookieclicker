import styled from 'styled-components/native';
import { normalize } from '@utils/functions/normalize';

export const DefaultText = styled.Text<{ size?: number }>`
    font-family: 'Bradley Hand';
    font-size: ${({ size, theme }) =>
        size ? normalize(size) : theme.fontSizes.m}px;
`;
