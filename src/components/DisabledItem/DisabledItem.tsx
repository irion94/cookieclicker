import IconManager from '@components/_universal/IconManager/IconManager';
import WhiteBoxText from '@components/_universal/WhiteBoxText/WhiteBoxText';
import React from 'react';
import styled from 'styled-components/native';

interface IProps {
    isShown: boolean;
    text: string;
}

export default ({ isShown, text }: IProps) => {
    if (isShown) {
        return (
            <Opacity>
                <IconManager type={'Lock'} />
                <WhiteBoxText>{text}</WhiteBoxText>
            </Opacity>
        );
    } else {
        return <></>;
    }
};

const Opacity = styled.View`
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: ${({ theme }) => theme.spacing.medium}px;
    background-color: #00000099;
    justify-content: center;
    align-items: center;
    border-radius: ${({ theme }) => theme.borderRadius.small}px;
`;
