import React, { ReactNode } from 'react';
import { StatusBar } from 'react-native';
import * as Styled from './App.styled';
import { ThemeProvider } from 'styled-components';
import BakeryStore from '@state/Bakery.store';
import StoreStore from '@state/Store.store';
import themeVariant from '@utils/themeVariant';
import { Theme } from '@@types/CommonTypes';
import RootStack from '../navigation/RootStack';
import { RootStateContext } from '@utils/hooks/useRootContext';

const App: () => ReactNode = () => {
    return (
        <RootStateContext.Provider
            value={{
                bakery: BakeryStore,
                store: StoreStore,
            }}
        >
            <ThemeProvider theme={themeVariant[Theme.DEFAULT]}>
                <StatusBar barStyle={'default'} />
                <Styled.SafeAreaView>
                    <RootStack />
                </Styled.SafeAreaView>
            </ThemeProvider>
        </RootStateContext.Provider>
    );
};

export default App;
