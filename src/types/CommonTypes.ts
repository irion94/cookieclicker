import * as Icons from '@shared/SVGs';
import defaultTheme from '@shared/theme/default.theme';
import { StyleProp, ViewStyle } from 'react-native';

export enum Theme {
    DEFAULT = 'DEFAULT',
    DARK = 'DARK',
}

export type ThemeType = typeof defaultTheme;

export type IconType = keyof typeof Icons;

export type ViewStyleProp = StyleProp<ViewStyle>;
