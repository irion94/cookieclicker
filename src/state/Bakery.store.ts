import { addMiddleware, applySnapshot, cast, getEnv, types } from 'mobx-state-tree';
import { mstLog } from 'mst-log';
import state_persistor from '@utils/functions/state_persistor';

const initialState = {
    points: 0,
    level: 1,
    multiplier: 1,
    robots: 0,
    robots_efficiency: 1,
    nextLevelRequirePoints: 10,
};

export const Bakery = types
    .model('Bakery', initialState)
    .actions((self) => ({
        afterCreate: () => {
            addMiddleware(self, mstLog());
            state_persistor('Bakery', self);
        },
        addPoints: (points: number) => {
            if (self.points + points >= 0) {
                self.points = self.points + points;
            }
        },
        setNextLevel: () => {
            self.level++;
            self.nextLevelRequirePoints = self.nextLevelRequirePoints * 2;
        },
        clearBakery: () => {
            applySnapshot(self, initialState);
        },
    }))
    .actions((self) => ({
        addMultiplier: (cost: number, callbacks?: () => void) => {
            if (self.points >= cost) {
                self.addPoints(-cost);
                self.multiplier++;
                callbacks?.();
            }
        },
        addRobots: (cost: number) => {
            console.log(self.points, cost, self.points >= cost);
            if (self.points >= cost) {
                self.addPoints(-cost);
                self.robots++;
            }
        },
        robotUpgrade: (cost: number) => {
            if (self.points >= cost) {
                self.addPoints(-cost);
                self.robots_efficiency = self.robots_efficiency * 0.9;
            }
        },
    }))
    .views((self) => ({
        get getNextLevel() {
            return self.nextLevelRequirePoints - self.points;
        },
        get getEfficiency() {
            return (self.robots / self.robots_efficiency).toPrecision(3);
        },
    }));

const BakeryStore = Bakery.create();

export default BakeryStore;
