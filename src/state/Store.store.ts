import {
    addMiddleware,
    applySnapshot,
    cast,
    getEnv,
    TypeOfValue,
    types,
} from 'mobx-state-tree';
import { mstLog } from 'mst-log';
import { StoreItemModel } from '@state/./StoreItem.store';
import { mock_articles } from '@shared/mockData';
import state_persistor from '@utils/functions/state_persistor';

const Store = types
    .model('Store', {
        articles: types.optional(types.array(StoreItemModel), []),
    })
    .actions((self) => ({
        setArticles(articles: TypeOfValue<typeof StoreItemModel>) {
            self.articles = articles;
        },
        clearStore() {
            applySnapshot(self, {
                articles: cast(getEnv(self).getArticlesApi()),
            });
        },
    }))
    .actions((self) => ({
        afterCreate() {
            addMiddleware(self, mstLog());
            state_persistor('Store', self).then(() => {
                if (!self.articles.length) {
                    const articles = getEnv(self).getArticlesApi();
                    //FIXME: override types
                    self.setArticles(
                        cast(articles) as TypeOfValue<typeof StoreItemModel>,
                    );
                }
            });
        },
    }));

const getArticlesApi = () => {
    return mock_articles;
};

const StoreStore = Store.create({ articles: [] }, { getArticlesApi });

export default StoreStore;
