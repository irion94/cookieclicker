import { addMiddleware, Instance, types } from 'mobx-state-tree';
import { mstLog } from 'mst-log';
import state_persistor from '@utils/functions/state_persistor';

export const StoreItemModel = types
    .model('StoreItemModel', {
        id: types.number,
        name: types.string,
        icon_name: types.maybe(types.string),
        cost: types.number,
        min_lvl: types.number,
        type: types.string,
    })
    .actions((self) => ({
        afterCreate: () => {
            state_persistor(`StoreItemModel[${self.id}]`, self);
            addMiddleware(self, mstLog());
        },
        bumpMinLvl: () => {
            self.min_lvl++;
            self.cost = self.cost * 2;
        },
    }));

export type IStoreItemValue = Instance<typeof StoreItemModel>;
