import React from 'react';
import styled from 'styled-components/native';
import Button from '@components/_universal/Button/Button';
import { useRootStore } from '@utils/hooks/useRootContext';
import { observer } from 'mobx-react';
import { clearStore } from '@utils/functions/state_persistor';
import { useNavigation } from '@react-navigation/native';

export default observer(() => {
    const { bakery, store } = useRootStore();
    const { navigate } = useNavigation();

    const onPress = () => {
        clearStore()
            .then(() => {
                bakery.clearBakery();
                store.clearStore();
            })
            .then(() => navigate('Game'))
            .catch((e) => console.log('Error while cache cleaning', e));
    };

    return (
        <Wrapper>
            <Button onPress={onPress}>Reset Game Data</Button>
        </Wrapper>
    );
});

const Wrapper = styled.View`
    flex: 1;
    background-color: ${({ theme }) => theme.colors.app};
    padding: 10px;
    justify-content: center;
`;
