import * as React from 'react';
import Cookie from '@components/Cookie/Cookie';
import styled from 'styled-components/native';
import useGameLogic from './useGameLogic';
import Points from '@components/Points/Points';
import { observer } from 'mobx-react';
import StoreList from '@components/StoreList/StoreList';

export default observer(() => {
    useGameLogic();

    return (
        <Wrapper>
            <Points />
            <Cookie />
            <StoreList />
        </Wrapper>
    );
});

const Wrapper = styled.View`
    flex: 1;
    background-color: ${({ theme }) => theme.colors.app};
    justify-content: space-between;
`;
