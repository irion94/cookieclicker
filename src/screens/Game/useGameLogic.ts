import { useEffect } from 'react';
import { useRootStore } from '@utils/hooks/useRootContext';

export default () => {
    const {
        bakery,
        bakery: { robots, robots_efficiency, nextLevelRequirePoints },
    } = useRootStore();
    const { points } = bakery;

    useEffect(() => {
        if (points >= nextLevelRequirePoints) {
            bakery.setNextLevel();
        }
    }, [bakery, points, nextLevelRequirePoints]);

    useEffect(() => {
        if (robots) {
            const interval = setInterval(() => {
                bakery.addPoints(robots);
            }, 1000 * robots_efficiency);
            return () => clearInterval(interval);
        }
        return () => null;
    }, [bakery, robots, robots_efficiency]);

    return {};
};
