import persist from 'mst-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const clearStore = async () => {
    await AsyncStorage.clear().then(() => console.log('Cache cleared!'));
};

export default (storeName: string, store: any) => {
    return persist(storeName, store, {
        storage: AsyncStorage,
        jsonify: true,
    }).then(() => console.log(`${storeName} has been hydrated`));
};
