import { useValue, timing, Easing } from 'react-native-reanimated';

export const useAnimation = () => {
    const scale = useValue(1);

    const scaleAnimation = () =>
        timing(scale, {
            toValue: 1.3,
            duration: 150,
            easing: Easing.inOut(Easing.bounce),
        }).start(({ finished }) => {
            if (finished)
                timing(scale, {
                    toValue: 1,
                    duration: 100,
                    easing: Easing.out(Easing.bounce),
                }).start();
        });

    return { scale, scaleAnimation };
};
