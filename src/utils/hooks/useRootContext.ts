import { createContext, useContext } from 'react';
import BakeryStore from '@state/Bakery.store';
import StoreStore from '@state/Store.store';

type RootStateContextValue = {
    bakery: typeof BakeryStore;
    store: typeof StoreStore;
};

export const RootStateContext = createContext<RootStateContextValue>(
    {} as RootStateContextValue,
);

export const useRootStore = () => useContext(RootStateContext);
