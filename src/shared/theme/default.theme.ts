import { palette, global } from './config';

const defaultTheme = {
    colors: {
        app: palette.main_bg,
        secondary: palette.overlay,
        box: palette.dark_green,
        drawer: palette.overlay,
        button: palette.white,
    },
    ...global,
};

export default defaultTheme;
