import { normalize } from '@utils/functions/normalize';

export const palette = {
    black: '#000000',
    white: '#FFFFFF',
    main_bg: '#4DB8C1',
    overlay: '#1298A0',
    dark_green: '#079FA4',
};

export const global = {
    fontSizes: {
        m: normalize(25),
    },
    fontWeight: {
        light: 300,
        regular: 400,
        bold: 700,
        black: 900,
    },
    borderRadius: {
        small: 5,
        default: 10,
        switch: 15,
    },
    transitions: {
        default: 0.5,
    },
    spacing: {
        narrow: 5,
        medium: 10,
        wide: 15,
        list_gap: 10,
        input_gap: 20,
    },
};
