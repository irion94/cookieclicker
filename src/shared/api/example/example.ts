import createRequest from '@services/ApiService';

export default {
    get: () => createRequest<any>(`/example`),
    getSingle: (id: number) => createRequest<any>(`/example/${id}`),
    create: (data: any) => createRequest<any>(`/example`, 'POST', data),
    update: (data: Partial<any>) =>
        createRequest<any>(`/example`, 'PATCH', data),
    delete: (id: number) => createRequest<any>(`/example/${id}`, 'DELETE'),
};
