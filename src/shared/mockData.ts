import { IStoreItemValue } from '@state/StoreItem.store';

export const mock_articles: Array<Omit<
    IStoreItemValue,
    'bumpMinLvl' | 'afterCreate'
>> = [
    {
        id: 1,
        name: 'Multiplier',
        cost: 15,
        icon_name: 'Pointer',
        min_lvl: 2,
        type: 'multiply',
    },
    {
        id: 2,
        name: 'Robot',
        cost: 100,
        icon_name: 'Robot',
        min_lvl: 5,
        type: 'robot',
    },
    {
        id: 3,
        name: 'Robot Upgrade',
        cost: 200,
        icon_name: 'Flash',
        min_lvl: 8,
        type: 'robot_upgrade',
    },
];
