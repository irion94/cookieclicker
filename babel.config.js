module.exports = {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
        ['@babel/plugin-transform-flow-strip-types'],
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        ['@babel/plugin-proposal-class-properties', { loose: true }],
        [
            'module-resolver',
            {
                root: ['./'],
                extensions: [
                    '.ios.ts',
                    '.android.ts',
                    '.ts',
                    '.ios.tsx',
                    '.android.tsx',
                    '.tsx',
                    '.jsx',
                    '.js',
                    '.json',
                ],
                alias: {
                    '@src': './src',
                    '@assets': './assets',
                    '@assets/*': 'assets/*',
                    '@components/*': 'src/components/*',
                    '@containers/*': 'src/containers/*',
                    '@domains/*': 'src/domains/*',
                    '@routes/*': 'src/routes/*',
                    '@services/*': 'src/services/*',
                    '@state/*': 'src/state/*',
                    '@shared/*': 'src/shared/*',
                    '@utils/*': 'src/utils/*',
                    '@@types/*': 'src/types/*',
                },
            },
        ],
    ],
};
