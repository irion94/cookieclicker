<h1>Cookie Clicker</h1>

## This project uses yarn. **_Don't use `npm` commands!!!_**

<dl>
    <dt>Scripts:</dt>
    <dd>Mac users -> install cocoapods https://cocoapods.org/</dd>
    <dd>yarn install</dd>
    <dd>cd ios -> pod install</dd>
</dl>
<dl>
    <dt>Run:</dt>
    <dd>Optional: watchman watch-del-all</dd>
    <dd>yarn start / yarn start --reset-cache</dd>
    <dd>react-native run-ios -option: --simulator "iPhone 11"</dd>
    <dd>react-native run-android</dd>
</dl>
